<?php

class Produto {
    private $id, $name, $sku, $price, $desc, $qty;

    public function __construct($id, $name, $sku, $price, $desc, $qty) {
        $this->id = $id;
        $this->name = $name;
        $this->sku = $sku;
        $this->price = $price;
        $this->desc = $desc;
        $this->qty = $qty;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * @return mixed
     */
    public function getQty()
    {
        return $this->qty;
    }

    public function getAll()
    {
        return array(
            "id" => $this->id,
            "nome" => $this->name,
            "sku" => $this->sku,
            "preco" => $this->price,
            "descricao" => $this->desc,
            "quantidade" => $this->qty
        );
    }
}

?>