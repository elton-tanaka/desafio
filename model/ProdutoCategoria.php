<?php

class ProdutoCategoria
{
    private $product_id, $category_id;

    public function __construct($product_id, $category_id)
    {
        $this->product_id = $product_id;
        $this->category_id = $category_id;
    }

    public function getAll() {
        return array (
            "produto_id" => $this->product_id,
            "categoria_id" => $this->category_id
        );
    }
}