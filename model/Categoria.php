<?php

class Categoria {
    private $id, $name, $code;

    public function __construct($id, $name, $code) {
        $this->id = $id;
        $this->name = $name;
        $this->code = $code; 
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function getCode() {
        return $this->code;
    }
    
    public function getAll() {
        return array(
            "id"    => $this->id,
            "nome"  => $this->name,
            "codigo"  => $this->code
        );

        //return $this;
    }
}

?>