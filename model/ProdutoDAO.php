<?php

require_once 'baseDAO.php';
require_once 'Produto.php';
require_once 'ProdutoCategoria.php';


class ProdutoDAO extends baseDAO {

    private $table = array(
        "name" => "produto",
        "rows" => [
            "nome"      => ":nome",
            "sku"       => ":sku",
            "preco"     => ":preco",
            "descricao" => ":descricao",
            "quantidade" => ":quantidade"
        ]
    );

    private $joinedTable = array(
        "name" => "produto_categoria",
        "rows" => [
            "produto_id" => ":produto_id",
            "categoria_id" => ":categoria_id"
        ]
    );

    public function createProduct($product, $categoriesIds) {

        if($productId = $this->create($this->table, $product)) { //returns false if query fails
            foreach ($categoriesIds as $categoryId) {
                $obj = new ProdutoCategoria($productId, $categoryId);
                $this->addCategoryToProduct($obj);
            }
            return true;
        } else return false;
    }

    public function fetchProduct($id = NULL) {
        $products = new ArrayObject();

        foreach ($this->read($this->table, $id) as $row) {

            $product = new Produto($row['id'], $row['nome'], $row['sku'], $row['preco'], $row['descricao'], $row['quantidade']);
            $products->append($product);
        }
        return $products;

    }

    public function fetchCategories($id) {
        $query = "SELECT categorias.nome FROM produto_categoria RIGHT JOIN categorias ON produto_categoria.categoria_id = categorias.id WHERE produto_id = " . $id;

        try{
            $conn = new Connection();
            $stmt = $conn->getConn();

            return $stmt->query($query);


        } catch (Exception $e) {
            //TODO log
            $error_message = $e->getMessage();
            echo $error_message;
        }
    }

    public function editProduct($product, $categoriesIds) {
        $query = "DELETE FROM produto_categoria WHERE produto_id = " . $product->getId();

        try {
            $conn = new Connection();
            $stmt = $conn->getConn()->prepare($query);
            $stmt->execute();

            if ($this->update($this->table, $product)) {
                foreach ($categoriesIds as $categoryId) {
                    $obj = new ProdutoCategoria($product->getId(), $categoryId);
                    $this->addCategoryToProduct($obj);
                }
                return true;
            }



        } catch (Exception $e) {
            $error_message = $e->getMessage();
            echo $error_message;
        }


    }

    public function deleteProduct($id) {
        return $this->delete($this->table, $id);
    }

    public function addCategoryToProduct($categoryObj) {
        return $this->create($this->joinedTable, $categoryObj);
    }
}
?>