<?php

$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/desafio/db/Connection.php";
require($path);

abstract class baseDAO {

    //$table structure should come like this:
    //$table->name == produto
    //$table->rows == [
    //  "nome" => ":nome"
    //  "sku" => ":sku"
    //  "preco" => ":preco"
    //  ...
    //]

    public function create($table, $obj) {

        $query = "INSERT INTO " . $table["name"] . " (" . implode(", " , array_keys($table["rows"])) . ")" . " VALUES (" . implode(", ", array_values($table["rows"])) . ")";
        //ex: INSERT INTO produtos (nome, sku, preco, descricao, quantidade) VALUES (:nome, :sku, :preco, :descricao, :quantidade)

        try {
            $conn = new Connection();
            $stmt = $conn->getConn()->prepare($query);

            foreach ($obj->getAll() as $key => $value) {//note $obj doesnt have an id yet
                if(empty($value)) continue;
                $stmt->bindValue($table["rows"][$key], $value); //change :values to respective object values
            }

            if($stmt->execute()) { //if query works
                return $conn->getConn()->lastInsertId();
            }else return false;

        } catch (Exception $e) {
            //TODO log
            $error_message = $e->getMessage();
            echo $error_message;
        }
    }

    public function read($table, $id = NULL) {
        $query = "SELECT * FROM " . $table["name"];
        if (isset($id)) {
            $query .= " WHERE id = " . $id;
        }
        //ex: SELECT * FROM categoria WHERE id = 2
        try{
            $conn = new Connection();
            $stmt = $conn->getConn();

            return $stmt->query($query);

        } catch (Exception $e) {
            //TODO log
            $error_message = $e->getMessage();
            echo $error_message;
        }
    }

    public function update($table, $obj) {
        $query = "UPDATE " . $table["name"] . " SET " . $this->mapped_implode(', ', $table["rows"], ' = ') . " WHERE id = :id";
        //ex: UPDATE produto SET nome = :nome, sku = :sku, preco = :preco, descricao = :descricao, quantidade = :quantidade WHERE id = :id

        try{
            $conn = new Connection();
            $stmt = $conn->getConn()->prepare($query);
            $isId = true; //small workaround to skip first iteration

            foreach ($obj->getAll() as $key => $value) {
                if ($isId) { //skips first iteration
                    $isId = false;
                    continue;
                }
                $stmt->bindValue($table["rows"][$key], $value);
            }
            $stmt->bindValue(":id", $obj->getId());
            return $stmt->execute();
        } catch (Exception $e) {
            //TODO log
            $error_message = $e->getMessage();
            echo $error_message;
        }
    }

    public function delete($table, $id) {
        $query = "DELETE FROM " . $table["name"] . " WHERE id = :id";
        //ex: DELETE FROM categoria WHERE id = :id

        try{
            $conn = new Connection();
            $stmt = $conn->getConn()->prepare($query);

            $stmt->bindValue(":id", $id);
            $result = $stmt->execute();
            $this->updateProductCategory();

            return $result;
        } catch (Exception $e) {
            //TODO log
            $error_message = $e->getMessage();
            echo $error_message;
        }

    }

    public function updateProductCategory() {
        try{
            $conn = new Connection();

            $query = "DELETE FROM produto_categoria 
                    WHERE NOT EXISTS(SELECT NULL
                    FROM categorias c
                    WHERE c.id = categoria_id)";

            $stmt = $conn->getConn()->prepare($query);
            $stmt->execute();

            $query = "DELETE FROM produto_categoria 
                    WHERE NOT EXISTS(SELECT NULL
                    FROM produto p
                    WHERE p.id = produto_id)";

            $stmt = $conn->getConn()->prepare($query);
            $stmt->execute();
        } catch (Exception $e) {
            //TODO log
            $error_message = $e->getMessage();
            echo $error_message;
        }
    }

    
    private function mapped_implode($glue, $array, $symbol = '=') { //used on update() to set query
        return implode($glue, array_map(
                function($k, $v) use($symbol) {
                    return $k . $symbol . $v;
                },
                array_keys($array),
                array_values($array)
                )
            );
    }
}

?>