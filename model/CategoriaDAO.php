<?php

require_once 'baseDAO.php';
require_once 'Categoria.php';

class CategoriaDAO extends baseDAO {

    private $table = array(
        "name" => "categorias",
        "rows" => [
            "nome"      => ":nome",
            "codigo"    => ":codigo"
            ]
    );

    public function createCategory($category) {
        return $this->create($this->table, $category);
    }

    public function fetchCategory($id = NULL) {
        $categories = new ArrayObject();
        foreach ($this->read($this->table, $id) as $row) {
            $category = new Categoria($row['id'], $row['nome'], $row['codigo']);
            $categories->append($category);
        }

        return $categories;
    }

    public function editCategory($category) {
        return $this->update($this->table, $category);
    }

    public function deleteCategory($id) {
        return $this->delete($this->table, $id);
    }
}

?>