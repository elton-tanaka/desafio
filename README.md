# Desafio Backend Webjump

### Requisitos:
<ul>
    <li>PHP 7.4</li>
    <li>MySQL</li>
    <li>XAMPP</li>
</ul>

### Instalação

Clonar o repositorio no diretorio do xampp.

Criar um banco de dados chamado **webjumpdb**

Utilizar o script sql para criação das tabelas.

Iniciar no localhost.


