<?php

    include_once '../model/Produto.php';
    include_once '../model/ProdutoDAO.php';

    if(!empty($_POST)) {
        if(isset($_POST['add-product'])) {

            $product = new Produto('', $_POST['name'], $_POST['sku'], $_POST['price'], $_POST['desc'], $_POST['qty']);
            $productDAO = new ProdutoDAO();
            $categoriesIds = $_POST['category'];
            $result = $productDAO->createProduct($product, $categoriesIds);

            if($result){
                echo '<script> '
                    . 'alert("Successfully created");'
                    . 'window.location.href = "/desafio/products.php"'
                    . '</script>';
            } else {
                echo '<script> '
                    . 'alert("Oops Something went wrong");'
                    . 'window.location.href = "/desafio/products.php"'
                    . '</script>';
            }
        } elseif(isset($_POST['edit-product'])) {

            $product = new Produto($_POST['id'], $_POST['name'], $_POST['sku'], $_POST['price'], $_POST['desc'], $_POST['qty']);
            $productDAO = new ProdutoDAO();
            $categoriesIds = $_POST['category'];
            $result = $productDAO->editProduct($product, $categoriesIds);

            if($result){
                echo '<script> '
                    . 'alert("Successfully edited");'
                    . 'window.location.href = "/desafio/products.php"'
                    . '</script>';
            } else {
                echo '<script> '
                    . 'alert("Oops Something went wrong");'
                    . 'window.location.href = "/desafio/products.php"'
                    . '</script>';
            }

        }
    } elseif(!empty($_GET)) {
        if (isset($_GET['delete-product']) && $_GET['delete-product'] == "true") {
            $productDao = new ProdutoDAO();
            $result = $productDao->deleteProduct($_GET['id']);
            if($result){
                echo '<script> '
                    . 'alert("Product successfully deleted");'
                    . 'window.location.href = "/desafio/products.php"'
                    . '</script>';
            } else {
                echo '<script> '
                    . 'alert("Oops Something went wrong");'
                    . 'window.location.href = "/desafio/products.php"'
                    . '</script>';
            }
        }

    }