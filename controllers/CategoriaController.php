<?php
    include_once '../model/Categoria.php';
    include_once '../model/CategoriaDAO.php';

    if(!empty($_POST)) {
        if(isset($_POST['add-category'])) {
            $category = new Categoria('', $_POST['category-name'], $_POST['category-code']);
            $categoryDao = new CategoriaDAO();
            $result = $categoryDao->createCategory($category);
            if($result){
                echo '<script> '
                    . 'alert("Successfully created");'
                    . 'window.location.href = "/desafio/categories.php"'
                    . '</script>';
            } else {
                echo '<script> '
                    . 'alert("Oops Something went wrong");'
                    . 'window.location.href = "/desafio/categories.php"'
                    . '</script>';
            }
        } elseif(isset($_POST['edit-category'])) {
            $category = new Categoria($_POST['category-id'], $_POST['category-name'], $_POST['category-code']);
            $categoryDao = new CategoriaDAO();
            $result = $categoryDao->editCategory($category);
            if($result){
                echo '<script> '
                    . 'alert("Successfully edited");'
                    . 'window.location.href = "/desafio/categories.php"'
                    . '</script>';
            } else {
                echo '<script> '
                    . 'alert("Oops Something went wrong");'
                    . 'window.location.href = "/desafio/categories.php"'
                    . '</script>';
            }
        }
    } elseif(!empty($_GET)) {
        if (isset($_GET['delete-category']) && $_GET['delete-category'] == "true") {
            echo "delete";
            $categoryDao = new CategoriaDAO();
            $result = $categoryDao->deleteCategory($_GET['id']);
            if($result){
                echo '<script> '
                    . 'alert("Bye Bye!");'
                    . 'window.location.href = "/desafio/categories.php"'
                    . '</script>';
            } else {
                echo '<script> '
                    . 'alert("Oops Something went wrong");'
                    . 'window.location.href = "/desafio/categories.php"'
                    . '</script>';
            }
        }

    }