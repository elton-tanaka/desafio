<?php

    Class Connection {

        private $hostname = "localhost";
        private $username = "root";
        private $password = "";
        private $dbName = "webjumpdb";

        private $conn;
        
        public function __construct()
        {
            try {
                $db = "mysql:host=" . $this->hostname . ";dbname=" . $this->dbName ;
                $this->conn = new PDO($db, $this->username, $this->password);
//                echo "Connected";

                //TODO log conn
            } catch(PDOException $e) {
                $error_message = $e->getMessage();
                echo $error_message;
                // Todo log error
                exit();
            }
        }
        
        public function getConn() {
            return $this->conn;
        }
    }
?>
